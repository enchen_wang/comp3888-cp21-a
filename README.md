# **COMP3888 CodeCombat Project**#
A Tower Defence arena level on the CodeCombat platform: Heart Guardian.

# Access Our Game#
[Heart Guardian](https://codecombat.com/play/level/treacherous-tower-defense)

# Code Documentation#
[Doc](https://docs.google.com/document/d/12LevPmwDIMX8fXEV35xJ0ztpuHsBp8I9d_91uKTA-Mg/edit?usp=sharing)

# How To Play#
## Programming language##
Python

## Target users##
CS1-6 & GD1 CodeCombat Students

## Goal##
Defend your heart and survive for as long as possible

In this arena, you control a hero who will fight against incomeing waves of enemy units.

Your hero has the ability to spend gold to spawn allied units who will help in this battle.

Gold is earned by killing enemies.

Traps can also be used to damage enemies.

Build fences to slow down the enemy.

Potions can be collected to heal your heart.

After 85 seconds, if player and cpu both alive, who get more money who will win the game.

## Methods to use##
* hero.say("Type,Position"): summon a unit on the specfic position
* hero.buildXY("Item",x,y): build an item on (x,y) points.


## Units we can summon and the cost##
+ warrior: 30 gold
+ knight: 25 gold
+ thief: 30 gold
+ wizard: 40 gold
+ archer: 30 gold
+ thrower: 20 gold
+ buffer: 70 gold
+ warlock: 100 gold

## Items we can build and the cost##
+ palisade: 5 gold
+ fire-trap: 50 gold
